
DROP TABLE IF EXISTS claim_details;
CREATE TABLE claim_details (
	identity varchar(255),
    policy_request JSON,
    premium INT,
    claim_number VARCHAR(100),
    claim_amount INT,
    claim_status VARCHAR(100)
);

INSERT INTO claim_details SELECT I.identity, I.policy_request, Cp.premium, C.claim_number, C.claim_amount, C.claim_status
FROM insurances AS I, coverage_periods AS Cp, claims AS C
WHERE I.identity = Cp.identity AND I.identity = C.identity AND C.claim_number != "";