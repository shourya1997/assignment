DROP TABLE IF EXISTS claims;
CREATE TABLE claims (
    identity INT NOT NULL,
    claim_number VARCHAR(100),
    claim_created_at VARCHAR(100),
    claim_amount INT,
    claim_status VARCHAR(100)
);

LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/claims.csv' 
INTO TABLE claims 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

DROP TABLE IF EXISTS insurances;
CREATE TABLE insurances (
    identity INT NOT NULL,
    policy_request JSON,
    policy_created_at VARCHAR(100)
);
LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/insurances.csv' 
INTO TABLE insurances 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

DROP TABLE IF EXISTS coverage_periods;
CREATE TABLE coverage_periods (
    identity INT NOT NULL,
    premium INT
);
LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/coverage_periods.csv' 
INTO TABLE coverage_periods 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

SELECT DATE_FORMAT(claim_created_at, '%Y-%m-%d %H:%i:%s') FROM claims;
SELECT DATE_FORMAT(policy_created_at, '%Y-%m-%d %H:%i:%s') FROM insurances;
