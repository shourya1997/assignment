import datetime as dt
import os

from airflow import DAG
from airflow.operators.mysql_operator import MySqlOperator

# export AIRFLOW_HOME='/home/shourya97/Personel/placement-assignments/pasarpolis/airflow_home'

CONNECTION_ID = 'mysql_flight'
DATABASE = 'airflow'
RULES_SQL = 'rules.sql'
INSERT_SQL = 'insert.sql'
UPDATE_SQL = 'update.sql'
SELECT_SQL = 'select.sql'

default_args = {
    'owner': 'Airflow',
    'depends_on_past': True,
    'start_date': dt.datetime(2019, 10, 24),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'concurrency': 1,
    'end_date': dt.datetime(2019, 10, 26)
}

with DAG('script',
         default_args=default_args,
         schedule_interval='*/10 * * * *',
         template_searchpath = ['/home/shourya97/Personel/placement-assignments/pasarpolis']
         ) as dag:
    
    t0 = MySqlOperator(
        task_id = 'rules',
        sql = RULES_SQL,
        autocommit = True,
        database = DATABASE,
        mysql_conn_id = CONNECTION_ID,
        dag = dag
    )
    
    t1 = MySqlOperator(
        task_id = 'insert_csv',
        sql = INSERT_SQL,
        autocommit = True,
        database = DATABASE,
        mysql_conn_id = CONNECTION_ID, 
        dag = dag
    )
    
    t2 = MySqlOperator(
        task_id = 'claim_details',
        sql = 'claim_details.sql',
        autocommit = True,
        database = DATABASE,
        mysql_conn_id = CONNECTION_ID,
        dag = dag
    )

    t3 = MySqlOperator(
        task_id = 'update_table',
        sql = UPDATE_SQL,
        autocommit = True,
        database = DATABASE,
        mysql_conn_id = CONNECTION_ID,
        dag = dag
    )

    t4 = MySqlOperator(
        task_id = 'select_table',
        sql = SELECT_SQL,
        autocommit = True,
        database = DATABASE,
        mysql_conn_id = CONNECTION_ID,
        dag = dag
    )
        
    t0 >> t1 >> t2 >> t3 >> t4

