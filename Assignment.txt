Assignment

We sell an insurance product called ‘flight-cancellation’. Upon purchase of this product, the person who has booked the flight has the option to cancel it as per her convenience up to 4 hours before the scheduled time of departure. We have observed a poor claim payout ratio overall and now, want to improve upon it.



As a step in that direction, we want to see the combination of flight and route (departure airport + destination airport) to note the ones which are performing poorly financially. We intend to increase their premium so that our profitability increases. Can you figure out a way to identify those? And then how do we increase their premium? Share that strategy



You have these 3 tables that I shared as CSV files -



Policies.csv
Claims.csv
Coverage_periods.csv


Upload these CSVs into the database as 3 separate tables using Airflow. You can set up MySQL/MS SQL server on your local machine. Prepare apt queries and results sets that help us solve the problem. Feel free to reach out to me in case of questions and validations or to share interim insights. We can have an intermediate call as well if you want it.



Please share your output as a CSV (again using Airflow) and the corresponding SQL queries by 11:59 PM, Thursday, January 23, 2019. We will do a call on Friday, January 24. Let me know what time works for you.