
SELECT *,COUNT(*) FROM claim_details
GROUP BY dep_flight_code
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(*) DESC
INTO OUTFILE '/var/lib/mysql-files/dep_flight_code.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT *,COUNT(*) FROM claim_details
GROUP BY route
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(claim_amount) DESC
INTO OUTFILE '/var/lib/mysql-files/route.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT *,COUNT(*) FROM claim_details
GROUP BY rfc
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(*) DESC
INTO OUTFILE '/var/lib/mysql-files/rfc.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

-- SHOW VARIABLES LIKE "secure_file_priv";