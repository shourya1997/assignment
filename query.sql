use airflow;
drop table claims;
CREATE TABLE claims (
    identity INT NOT NULL,
    claim_number VARCHAR(100),
    claim_created_at VARCHAR(100),
    claim_amount INT,
    claim_status VARCHAR(100)
);
LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/claims.csv' 
INTO TABLE claims 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
select * from claims limit 10;

drop table insurances;
CREATE TABLE insurances (
    identity INT NOT NULL,
    policy_request JSON,
    policy_created_at VARCHAR(100)
);
LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/insurances.csv' 
INTO TABLE insurances 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
select * from insurances limit 10;

drop table coverage_periods;
CREATE TABLE coverage_periods (
    identity INT NOT NULL,
    premium INT
);
LOAD DATA LOCAL INFILE '/home/shourya97/Personel/placement-assignments/pasarpolis/csv/coverage_periods.csv' 
INTO TABLE coverage_periods 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
select * from coverage_periods as cp limit 10;

SELECT DATE_FORMAT(claim_created_at, '%Y-%m-%d %H:%i:%s') FROM claims;
SELECT DATE_FORMAT(policy_created_at, '%Y-%m-%d %H:%i:%s') FROM insurances;

SELECT policy_request
FROM insurances where identity=45649;

SELECT policy_request->>'$.flight_information.airport_code' airport, count(policy_request) 
from insurances 
group by policy_request->>'$.flight_information.airport_code';

DROP TABLE claim_details;
CREATE TABLE claim_details (
	identity varchar(255),
    policy_request JSON,
    premium INT,
    claim_number VARCHAR(100),
    claim_amount INT,
    claim_status VARCHAR(100)
);

INSERT INTO claim_details SELECT I.identity, I.policy_request, Cp.premium, C.claim_number, C.claim_amount, C.claim_status
FROM insurances AS I, coverage_periods AS Cp, claims AS C
WHERE I.identity = Cp.identity AND I.identity = C.identity AND C.claim_number != "";

select * from claim_details;

ALTER TABLE claim_details ADD route VARCHAR(100) DEFAULT 'reducted' NOT NULL;
ALTER TABLE claim_details ADD dep_flight_code VARCHAR(100) DEFAULT 'reducted' NOT NULL;
ALTER TABLE claim_details ADD rfc VARCHAR(100) DEFAULT 'reducted' NOT NULL;

SET SQL_SAFE_UPDATES = 0;

UPDATE claim_details 
SET route = CONCAT(
				policy_request->>'$.flight_information.airport_code' ,
                '-', 
                policy_request->>'$.flight_information.destination_airport_code'
                );

SELECT DISTINCT policy_request->>'$.flight_information.departure_flight_code',
COUNT(policy_request->>'$.flight_information.departure_flight_code')
from claim_details
GROUP BY policy_request->>'$.flight_information.departure_flight_code';

UPDATE claim_details 
SET dep_flight_code = policy_request->>'$.flight_information.departure_flight_code';

UPDATE claim_details 
SET rfc = concat(route, " ",dep_flight_code);

SELECT DISTINCT policy_request->>'$.flight_information.return_flight_code',
COUNT(policy_request->>'$.flight_information.return_flight_code')
from claim_details
GROUP BY policy_request->>'$.flight_information.return_flight_code';

SELECT identity, COUNT(identity)
FROM claim_details
GROUP BY identity
HAVING COUNT(identity) > 1;

SELECT claim_number, COUNT(*)
FROM claim_details
GROUP BY claim_number;

SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

SELECT *,COUNT(*) FROM claim_details
GROUP BY route
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(*) DESC;

SELECT *,COUNT(*) FROM claim_details
GROUP BY dep_flight_code
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(*) DESC
INTO OUTFILE '/var/lib/mysql-files/dep_flight_code.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT *,COUNT(*) FROM claim_details
GROUP BY route
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(claim_amount) DESC
INTO OUTFILE '/var/lib/mysql-files/route.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT *,COUNT(*) FROM claim_details
GROUP BY rfc
HAVING claim_amount > (select AVG(claim_amount) from claim_details) AND COUNT(*) > 1
ORDER BY COUNT(*) DESC
INTO OUTFILE '/var/lib/mysql-files/rfc.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SHOW VARIABLES LIKE "secure_file_priv";
