ALTER TABLE claim_details ADD route VARCHAR(100) DEFAULT 'reducted' NOT NULL;
ALTER TABLE claim_details ADD dep_flight_code VARCHAR(100) DEFAULT 'reducted' NOT NULL;
ALTER TABLE claim_details ADD rfc VARCHAR(100) DEFAULT 'reducted' NOT NULL;

UPDATE claim_details 
SET route = CONCAT(
				policy_request->>'$.flight_information.airport_code' ,
                '-', 
                policy_request->>'$.flight_information.destination_airport_code'
                );

UPDATE claim_details 
SET dep_flight_code = policy_request->>'$.flight_information.departure_flight_code';

UPDATE claim_details 
SET rfc = concat(route, " ",dep_flight_code);